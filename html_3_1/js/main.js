document.getElementById('hello_text').textContent = 'はじめてのJavaScript';

let count = 0;
setInterval(() => {
  count++;
  document.getElementById('hello_text').textContent = 'はじめてのJavaScript(' + count + ')';
  const td_array = document.getElementsByTagName('td');
  let cells = [];
  let index = 0;
  for (let row = 0; row < 20; row++) {
    cells[row] = [];
    for (let col = 0; col < 10; col ++) {
        cells[row][col] = td_array[index];
        index++;
    }
  }
  for (let i = 0; i < 10; i++) {
    cells[19][i].className = '';
  }
  for (let row = 18; row >= 0; row--) {
    for (let col = 0; col < 10; col++) {
        if (cells[row][col].className !== '') {
            cells[row + 1][col].className = cells[row][col].className;
            cells[row][col].className = '';
        }
    }
  }
}, 1000);