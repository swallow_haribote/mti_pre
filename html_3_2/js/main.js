document.getElementById('hello_text').textContent = 'はじめてのJavaScript';

let count = 0;
let cells;

const blocks = {
  i: {
    class: "i",
    pattern: [
      [1, 1, 1, 1]
    ]
  },
  o: {
    class: "o",
    pattern: [
      [1, 1], 
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0], 
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1], 
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0], 
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0], 
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1], 
      [1, 1, 1]
    ]
  }
};

loadTable();
setInterval(() => {
  count++;
  document.getElementById('hello_text').textContent = 'はじめてのJavaScript(' + count + ')';
  if (hasFallingBlock()) {
    fallBlocks();
  } else {
    deleteRow();
    generateBlock();
  }
}, 1000);

function loadTable() {
  cells = [];
  const td_array = document.getElementsByTagName('td');
  let index = 0;
  for (let row = 0; row < 20; row++) {
    cells[row] = [];
    for (let col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];
      cells[row][col].blockNum = index;
      index++;
    }
  }
}

function fallBlocks() {
  for (let col = 0; col < 10; col++) {
    if (cells[19][col].blockNum === fallingBlockNum) {
      isFalling = false;
      return;
    }
  }
  for (let row = 18; row >= 0; row--) {
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
          isFalling = false;
          return;
        }
      }
    }
  }
  for (let row = 18; row >= 0; row--) {
    for (let col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

let isFalling = false;
function hasFallingBlock() {
  return isFalling;
}

function deleteRow() {
  // そろっている行を消す
}

let fallingBlockNum = 0;
function generateBlock() {
  const keys = Object.keys(blocks);
  const nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  const nextBlock = blocks[nextBlockKey];
  const nextFallingBlockNum = fallingBlockNum + 1;
  const pattern = nextBlock.pattern;
  for (let row = 0; row < pattern.length; row++) {
    for (let col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}

// function moveRight() {
//   // ブロックを右に移動させる
// }

// function moveLeft() {
//   // ブロックを左に移動させる
// }

